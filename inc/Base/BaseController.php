<?php


namespace Inc\Base;


class BaseController
{
    public $plugin_path;
    public $plugin_url;
    public $plugin;
    public $post_type;


    public function __construct()
    {
        $this->plugin_path = plugin_dir_path( dirname_r( __FILE__, 2 ) );
        $this->plugin_url = plugin_dir_url( dirname_r( __FILE__, 2 ) );
        $this->plugin = plugin_basename( dirname_r( __FILE__, 3 ) ) .'/datasheet-generator.php';
        $this->post_type  = 'estate';
    }
}
