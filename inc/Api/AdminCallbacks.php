<?php

namespace Inc\Api;

use Inc\Base\BaseController;

class AdminCallbacks extends BaseController
{
    public function toolEstate()
    {
        require_once "$this->plugin_path/templates/estate_settings.php";
    }

}
